Attribute Copy is a plugin for QGis.

The objective of this plugin is to easily copy the attributes of a feature in a layer to another feature in a different layer. Both layers must by of Point type and have the same attributes (only attribute content is copied).

Usage:
- open both layers;
- set the target layer (the one where you will paste the attributes) as "editable";
![ALT](/images/image1.jpg)
- press the plugin button;
- draw a rectangle by pressing (left button) and dragging the mouse over both points;
![ALT](/images/image2.jpg)
- after releasing the mouse button, the attributes content of the point in the origin layer will be copied to the point in the target layer.
![ALT](/images/image3.jpg)


Centro de Informação Geoespacial do Exército (www.igeoe.pt)

Portugal
