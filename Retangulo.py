# -*- coding: utf-8 -*-

from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import QMessageBox

from qgis.gui import QgsMessageBar, QgsMapToolEmitPoint, QgsRubberBand, QgsVertexMarker
from qgis.core import QgsWkbTypes, QgsPointXY, QgsPoint, QgsRectangle, QgsFeatureRequest, QgsMapLayer, Qgis, QgsGeometry
from qgis.utils import iface 


class Retangulo(QgsMapToolEmitPoint): 
           
    #def __init__(self, canvas, iface):

    def __init__(self, canvas, iface):
        
        
        """
        draws a rectangle, then features that intersect this rectangle are added to selection 
        """
        # Store reference to the map canvas
        self.canvas = canvas  

        self.iface = iface        # add iface for work with active layer                     
                
        QgsMapToolEmitPoint.__init__(self, self.canvas)
                
        self.rubberBand = QgsRubberBand(self.canvas, geometryType=QgsWkbTypes.LineGeometry)
        self.rubberBand.setColor(QColor(0, 0, 240, 100))
        self.rubberBand.setWidth(1)
        
        self.reset() 
               

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False        
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )


    def canvasPressEvent(self, e):
        """
        Method used to build rectangle if shift is held, otherwise, feature select/deselect and identify is done.
        """
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        
        self.showRect(self.startPoint, self.endPoint)
        
    #self.isEmittingPoint is used to build the rubberband on the method on CanvasMoveEvent
            
    def canvasMoveEvent(self, e):
        """
        Used only on rectangle select.
        """
        if not self.isEmittingPoint:
            return
        self.endPoint = self.toMapCoordinates( e.pos() )
        self.showRect(self.startPoint, self.endPoint)

   
    def showRect(self, startPoint, endPoint):
        """
        Builds rubberband rect.
        """
        self.rubberBand.reset( QgsWkbTypes.PolygonGeometry )
        if startPoint.x() == endPoint.x() or startPoint.y() == endPoint.y():
            return
        
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(startPoint.x(), endPoint.y())
        point3 = QgsPointXY(endPoint.x(), endPoint.y())
        point4 = QgsPointXY(endPoint.x(), startPoint.y())

        self.rubberBand.addPoint(point1, False)
        self.rubberBand.addPoint(point2, False)
        self.rubberBand.addPoint(point3, False)
        self.rubberBand.addPoint(point4, True)    # true to update canvas
        self.rubberBand.show()


    def rectangle(self):
        """
        Builds rectangle from self.startPoint and self.endPoint
        """
        if self.startPoint is None or self.endPoint is None:
            return None
        elif self.startPoint.x() == self.endPoint.x() or self.startPoint.y() == self.endPoint.y():
            return None
        return QgsRectangle(self.startPoint, self.endPoint)

    
    def canvasReleaseEvent(self, e):
        """
        After the rectangle is built, here features are selected.
        """
        self.isEmittingPoint = False

        rect = self.rectangle()            #QgsRectangle
        
        if rect is not None:                        
                               
            layerActive = self.iface.activeLayer()   # work with active layer  
            
            featsActive = []             # feature of layer active
            pointsActive = []            # points of feature active
            pointsNActive = []           # points of feature not active
            ffeatsActive = []   
            ffeatsNActive = []   

            layers = self.iface.mapCanvas().layers() 
            
            for layer in layers:            #iterate all layers                
                if layer.type() != QgsMapLayer.VectorLayer or layer.geometryType() != QgsWkbTypes.PointGeometry: 
                    continue

                r = self.canvas.mapSettings().mapToLayerCoordinates(layerActive, rect)                
                
                request = QgsFeatureRequest(r)  
                for feat in layer.getFeatures(request):
             
                    geom = feat.geometry()

                    geomSingleType = QgsWkbTypes.isSingleType(geom.wkbType())                   
                    if geomSingleType:
                        elem = geom.asPoint()
                        check_point = QgsPointXY(elem)
                    else:
                        elem = geom.asMultiPoint()                    
                        check_point = QgsPointXY(elem[0])
                       
                    #  checks if the check_point is within the rectangle  and if it belongs to active layer  
                    if (r.xMinimum() <= check_point.x() <= r.xMaximum()) and (r.yMinimum() <= check_point.y() <= r.yMaximum()):                        
                        flagActive = 0
                                        
                        for activ in layerActive.getFeatures(request):                          
                            geom = activ.geometry()
                            
                            geomSingleType = QgsWkbTypes.isSingleType(geom.wkbType())                            
                            if geomSingleType:
                                elem1 = geom.asPoint()
                            else:
                                elem1 = geom.asMultiPoint()                             
                                                                                    
                            if elem1 == elem:      
                                flagActive = 1
                                pointsActive.append(check_point)                           
                                featsActive.append(feat.id())           # reserva feature por id    
                                ffeatsActive.append(feat)           # reserva feature    
                        if flagActive == 0: 
                            pointsNActive.append(check_point) 
                            ffeatsNActive.append(feat)           # reserva feature   
                     
            if len(pointsActive) == 0 or len(pointsNActive) == 0:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'The selection must have 2 points, one of them must belong to the active layer.')
                self.rubberBand.hide()
                return 

            if len(pointsActive) > 1:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'The selection must have 1 point of the active layer.')
                self.rubberBand.hide()
                return 

            if len(pointsNActive) > 1:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'The selection must have 1 point of the non-active layer.')
                self.rubberBand.hide()
                return 
       
            attrActive = ffeatsActive[0].attributes()

            attrNActive = ffeatsNActive[0].attributes()                
     
            indActive = 0 
            attrDict = {}
            for field in ffeatsActive[0].fields(): 
                indNActive = 0                

                for field1 in ffeatsNActive[0].fields():
                    if field1.name() == field.name() and field1.typeName() == field.typeName() and field1.length() == field.length():

                        attrDict[indActive] = attrNActive[indNActive]
                    indNActive += 1
                        
                indActive += 1  

            if len(attrDict) == 0:
                QMessageBox.information(self.iface.mainWindow(), "Error", 'The attribute names, types and length must be the same in both layers.')
                self.rubberBand.hide()
                return

            prov = layerActive.dataProvider() 

            prov.changeAttributeValues({featsActive[0] : attrDict})
           
            self.rubberBand.hide()
             
            layer.triggerRepaint()               
            self.iface.messageBar().pushMessage("CIGeoE Attribute Copy", "Attribute Copy done" , level=Qgis.Info, duration=2)
            
            self.iface.mapCanvas().refresh()



