# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CIGeoEAttributeCopy
                                 A QGIS plugin
 Copy the attribute from one feature to another (different layers)
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                             -------------------
        begin                : 2020-11-02
        copyright            : (C) 2020 by Centro de Informação Geoespacial do Exército 
        email                : igeoe@igeoe.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load CIGeoEAttributeCopy class from file CIGeoEAttributeCopy.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .CIGeoE_Attribute_Copy import CIGeoEAttributeCopy
    return CIGeoEAttributeCopy(iface)
